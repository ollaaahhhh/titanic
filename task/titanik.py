import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

df = pd.read_csv('titanic.csv')

def get_filled():
    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss"]
    result = []
    for title in titles:
        filtered_data = dataset[dataset['Name'].str.contains(title)]
        median_age = filtered_data['Age'].median()
        missing_values = filtered_data['Age'].isnull().sum()
        result.append((title, missing_values, round(median_age)))
    return result
